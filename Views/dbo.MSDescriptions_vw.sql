SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- select * from [MSDescriptions_vw] where tableName like 'spec%'
CREATE view [dbo].[MSDescriptions_vw]
as
with TableColumns as
(select tables.name TableName, columns.name columnName, isnull(columns.column_id, 0) ColumnOrder, id = tables.object_id, 
    Data_type = case when types.name like '%char%' or types.name like '%binary%' then types.name + '(' + cast(columns.max_length as varchar(10)) + ')'
      else types.name
      end
  , columns.precision, columns.scale, columns.is_nullable, columns.is_identity
  from sys.tables tables
    join sys.columns columns on columns.object_id = tables.object_id 
    join sys.types types on types.system_type_id = columns.system_type_id
  union all
  select s1.name TableName, null columnName, 0 ColumnOrder, s1.object_id, Data_type = 'Table', precision = null, scale = null, is_nullable = null, is_identity  = null
  from sys.tables s1
 ) 

select TableColumns.TableName,
 TableColumns.columnName,
 tableColumns.ColumnOrder,
 properties.value as descr,
 TableColumns.Data_type,
 TableColumns.precision,
 TableColumns.scale,
 TableColumns.is_nullable,
 TableColumns.is_identity
from TableColumns
 left join sys.extended_properties properties on TableColumns.id = properties.major_id 
   and properties.name = 'MS_DESCRIPTION'
   and TableColumns.ColumnOrder = properties.minor_id
--order by TableColumns.TableName , TableColumns.ColumnOrder
GO
