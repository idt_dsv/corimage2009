CREATE ROLE [IDT_WebApps]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_WebApps', N'IDT-CORALVILLE\IUSR_IDT'
GO
EXEC sp_addrolemember N'IDT_WebApps', N'IDT-CORALVILLE\SQLIsDevWeb'
GO
EXEC sp_addrolemember N'IDT_WebApps', N'webDisplay'
GO
